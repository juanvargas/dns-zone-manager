# Operations information

The TDZM Component generally does not require much related to operations. It is mainly managed through the TFM component and should not be used interactively by a human operator. There is one detail to watch out for:

## DNS Key Rollover:

To rollover the DNSSEC Keys it is necessary to completely remove the existing TDZM deployment. Please follow the steps outlined in the security concept here: https://gitlab.eclipse.org/eclipse/xfsc/train/TRAIN-Documentation/-/tree/main/concepts/security?ref_type=heads#zone-manager-tspa

And follow the last paragraph about DNSSEC Key Rollover.

## REST API
The Rest API is documented in [ZM_swagger.yaml](./ZM_swagger.yaml)