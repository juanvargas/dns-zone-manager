# Running:
If running the first time you **must** run `npm install`

Then run `npm run dev` to start the development mode server and page.

# Building:

*If you did not run `npm install` before (e.g. when running in dev mode) you must run now!*

Option 1: Build without docker.
- Run `npm run build` from the ui folder. The build output will be html/css/js files located in the `ui/dist` folder.
- Then run `npm start` which will start the code in production mode and serve the static dist folder.

Option 2: Build with docker
- run `npm run build` from the ui folder.
- run `docker build -t <your tag> .` from the ui folder.

When building with docker, the output will be a new image based on node:lts-alpine. This image will be configured to serve the build output files and REST routes through port 80.

# Configuration:

Following options show how to modify the UI when running it through the docker image.

**Changing the Logo:**

If you want to modify the look by changing the logo you should mount your image into the docker container. Example:
`docker run --name zone-manager-ui -p 8080:80 -v <local logo path>:/app/dist/logo.png <image-name>`

**Configuring general Configuration:**
Confgurations are loaded from a .env file in the docker image. The default .env can be overwritten by mounting your desired config:
`docker run --name zone-manager-ui -p 8080:80 -v <local config path>:/app/.env <image-name>`



## Configuration reference:
Fields you can set in the .env file **or as environment variables**

- ZONEMGR_URL (String) - Tells the UI where to fetch zone data from.
- API_KEY (String) - Tells the ui which api key to use to fetch the zone data.